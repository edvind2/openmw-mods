# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from portmod.pybuild import Pybuild1, InstallDir, File
from pyclass import CleanPlugin, NexusMod, CLEAN_DEPEND

EPIC_FILE = "Epic_Sadrith_Mora_Expanded-44113-1-0"


class Mod(CleanPlugin, NexusMod, Pybuild1):
    NAME = "Sadrith Mora Expanded"
    DESC = "Expands the city of Sadrith Mora, such as an upper district behind Tel Naga"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/44113"
    LICENSE = "all-rights-reserved"
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        tr? (
            landmasses/tamriel-data
            landmasses/tamriel-rebuilt
        )
        epic? (
            arch-towns/epic-sadrith-mora
        )
    """
    DEPEND = f"""
        base/morrowind
        tr? (
            landmasses/tamriel-data
            landmasses/tamriel-rebuilt
            {CLEAN_DEPEND}
        )
    """
    KEYWORDS = "openmw"
    SRC_URI = f"""
        Sadrith_Mora_Expanded-44113-1-0.rar
        {EPIC_FILE}.rar
    """
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/44113"
    IUSE = "tr epic"

    INSTALL_DIRS = [
        InstallDir(
            ".",
            PLUGINS=[
                File("sadrith mora expanded TR.esp", REQUIRED_USE="tr"),
                File("sadrith mora expanded.esp"),
            ],
            S="Sadrith_Mora_Expanded-44113-1-0",
            REQUIRED_USE="!epic",
        ),
        InstallDir(
            ".",
            PLUGINS=[
                File("Sadrith Mora Expanded-EpicComp.esp"),
                File("Sadrith Mora Expanded-EpicCompTR.esp", REQUIRED_USE="tr"),
            ],
            S="Epic_Sadrith_Mora_Expanded-44113-1-0",
            REQUIRED_USE="epic",
        ),
    ]

    def src_prepare(self):
        if "tr" in self.USE:
            if "epic" in self.USE:
                self.clean_plugin(
                    f"{self.WORKDIR}/{EPIC_FILE}/Sadrith Mora Expanded-EpicCompTR.esp"
                )
            else:
                self.clean_plugin("sadrith mora expanded TR.esp")
