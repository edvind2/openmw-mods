# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import shutil
from portmod.pybuild import Pybuild1, InstallDir, File

mainfile = "syc_herbalismforpurists-1.21"
patchfile = "Herbalism_for_Purists_v1.22_Fixed_ESP"


class Mod(Pybuild1):
    NAME = "Herbalism for Purists"
    DESC = "Lets you pick up plants instead of opening them as containers"
    HOMEPAGE = "http://mw.modhistory.com/download-1-9321"
    # Second source contains no license of any form
    LICENSE = "attribution all-rights-reserved"
    SRC_URI = f"""
        http://mw.modhistory.com/file.php?id=9321 -> {mainfile}.zip
        https://s3-us-west-2.amazonaws.com/modhistory/morrowind/additional/Herbalism%20for%20Purists%20v1.22%20Fixed%20ESP.7z
        -> {patchfile}.7z
    """
    RESTRICT = "mirror"
    RDEPEND = "morrowind"
    KEYWORDS = "openmw"
    IUSE = "bloodmoon tribunal"
    INSTALL_DIRS = [
        InstallDir(
            ".",
            PLUGINS=[
                File("Syc_HerbalismforPurists.esp"),
                File("Syc_HerbalismforPurists_BM.esp", REQUIRED_USE="bloodmoon"),
                File(
                    "Syc_HerbalismforPurists_TB.esp", REQUIRED_USE="tribunal bloodmoon"
                ),
                File(
                    "Syc_HerbalismforPurists_TB_ONLY.esp",
                    REQUIRED_USE="tribunal !bloodmoon",
                ),
            ],
            ARCHIVES=[File("Flora Glow.bsa")],
            S=mainfile,
        )
    ]

    def src_prepare(self):
        shutil.move(
            os.path.join(self.WORKDIR, patchfile, "Syc_HerbalismforPurists.esp"),
            os.path.join(self.WORKDIR, mainfile, "Syc_HerbalismforPurists.esp"),
        )
        Pybuild1.src_prepare(self)
