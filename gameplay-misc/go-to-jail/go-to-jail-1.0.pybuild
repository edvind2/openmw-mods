# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from portmod.pybuild import Pybuild1, InstallDir, File
from portmod.pybuild.modinfo import M
from pyclass import CleanPlugin


class Mod(CleanPlugin, Pybuild1):
    NAME = "Morrowind Go To Jail"
    DESC = "makes going to jail more like Oblivion"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/37792"
    LICENSE = "attribution-distribution"  # See nexus mods permissions info
    RDEPEND = "base/morrowind[bloodmoon?,tribunal?]"
    DEPEND = "base/morrowind"
    KEYWORDS = "openmw"
    SRC_URI = f"""
        {M}-mournhold-solstheim.zip
        {M}-solstheim.zip
        {M}-mournhold.zip
        {M}.zip
    """
    IUSE = "bloodmoon tribunal"

    INSTALL_DIRS = [
        InstallDir(
            ".",
            PLUGINS=[File("Go To Jail (Mournhold + Solshteim).ESP")],
            S=f"{M}-mournhold-solstheim",
            REQUIRED_USE="bloodmoon tribunal",
        ),
        InstallDir(
            ".",
            PLUGINS=[File("Go To Jail (Solstheim).ESP")],
            S=f"{M}-solstheim",
            REQUIRED_USE="bloodmoon !tribunal",
        ),
        InstallDir(
            ".",
            PLUGINS=[File("Go To Jail (Mournhold).ESP")],
            S=f"{M}-mournhold",
            REQUIRED_USE="tribunal !bloodmoon",
        ),
        InstallDir(
            ".",
            PLUGINS=[File("Go To Jail.esp")],
            S=f"{M}",
            REQUIRED_USE="!tribunal !bloodmoon",
        ),
    ]

    def src_prepare(self):
        if "bloodmoon" in self.USE and "tribunal" in self.USE:
            self.clean_plugin("Go To Jail (Mournhold + Solshteim).ESP")
        elif "bloodmoon" in self.USE and "tribunal" not in self.USE:
            self.clean_plugin("Go To Jail (Solstheim).ESP")
        elif "bloodmoon" not in self.USE and "tribunal" in self.USE:
            self.clean_plugin("Go To Jail (Mournhold).ESP")
        else:
            self.clean_plugin("Go To Jail.esp")
